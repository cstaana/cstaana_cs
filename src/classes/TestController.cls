public class TestController {

    public PageReference accountClicked() {
        contactsInformation = [SELECT FirstName, LastName FROM Contact 
            WHERE AccountID = :selectedAccount];
        
        return null;
    }


    public Id selectedAccount { get; set; }  
    public List<Contact> contactsInformation { get; set; }

    public List<Account> getMyAccounts() {
        return [SELECT Id, Name, AccountNumber FROM Account ORDER BY
            LastModifiedDate DESC LIMIT 10];
    }
}