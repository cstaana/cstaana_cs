public with sharing class TestTableController {

    public List<Account> accts {get;set;}
    public List<AccountWrapper> acctTable {get;set;}

    public TestTableController() {
        
        accts = [SELECT a.Id, a.Name, (SELECT Id FROM Contacts) FROM Account a];
        acctTable = new List<AccountWrapper>();
        for(Account a : accts) {
            AccountWrapper acctRow = new AccountWrapper();
            acctRow.Id = a.Id;
            acctRow.Name = a.Name;
            acctRow.contacts = new List<Contact>();
            for(Contact c : a.Contacts) {
                acctRow.contacts.add(c);
            }
            acctTable.add(acctRow);
        }
    }

    public class AccountWrapper {
        
        public String Id {get;set;}
        public String Name {get;set;}
        public List<Contact> contacts {get;set;}
    
    }
}