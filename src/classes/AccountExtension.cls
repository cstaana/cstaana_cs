public class AccountExtension {

    private final Account acct;
    private Account acctB;
    public String a;
    
    public AccountExtension(ApexPages.StandardController controller) {
        this.acct = (Account) controller.getRecord();
        this.acctB = [SELECT Name, Owner.FirstName 
                     FROM Account
                     WHERE Id = :acct.Id];
    }
    public String getTitle() {
        //return acct.Name;
        String a = '';
        List<String> stringList = new List<String>{'a', 'b', 'c', 'd', 'e', 'f', 'g'};
        Set<String> stringSet = new Set<String>{'a', 'b', 'c', 'd', 'e', 'f', 'g'};
        for(String s : stringSet) {
            a+=s;
        }
        
        return acctB.Owner.FirstName;
    }
    
    public PageReference stuff() {
    	update this.acct;
        return null;
    }

}